# The Madison Park Vocational Machine 

This is a medium-format machine for educational contexts. 

![mill](/images/mpvm-v03.jpg)

![motion](/video/mocontrol-mpvm.mp4)

This implements the [RCT Gantries v0](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/kunits) project for hardware design, and the [AutomataKit](https://gitlab.cba.mit.edu/jakeread/automatakit) project for controls. These operate via a [RuNDMC](https://gitlab.cba.mit.edu/jakeread/rndmc) controller.

The machine has a bed size of ```410 x 820 x 40mm (16.15 x 32.30 x 1.5")``` - the machine itself is 740 x 1100 x 430mm, and weighs about 20kg. It fits on one 4x8' sheet of HDPE, that we cut on our shopbot with a 1/8" single-flute o-cutter. The whole thing machines in about 2.5 hours, and goes together in a few afternoons or less. 

![milling](/images/mpvm-v03-jake-make.jpg)

# To-Dos

Two of these machines are near completion at Madison Park. Hurray! Here is a running list of things that need to happen to finish those machines:

**(1) Finish Soldering Stepper Motor Boards to Stepper Motors** 
 - The [stepper driver boards](https://gitlab.cba.mit.edu/jakeread/atkstepper23) need to be connected to the four wires that drive the motor. These wires can be soldered directly on to the circuit board, or you can solder a screw terminal into the stepper board and connect the motor via those terminals.

**(2) Route Power and Data to the Stepper Boards** 
 - There is documentation [at this link](https://gitlab.cba.mit.edu/jakeread/automatakit#usage) for connecting power and RJ45 cables to each motor. Keep in mind that cables need to move when the machine moves! 

**(3) Install and Connect the Power Supply**
 - The 24V 500W power supply switches AC 110v power into DC power. I normally connect it via an e-stop, as in, I put the E-Stop in-line with the Line phase of the wall supply. There is a hole near the front of the machine that should fit the e-stop, and you can use leftover 18ga wire to connect it to the power supply. All of the power leads from the motors should connect directly to the supply.

**(4) Install and Connect the Router** 
 - Top-level control happens in [RuNDMC](https://gitlab.cba.mit.edu/jakeread/rndmc) on the [router with a raspberry pi](https://gitlab.cba.mit.edu/jakeread/atkrouter/tree/master). Mount the router close to the terminal point for all of the RJ45 wires, and boot up the Raspberry Pi! This should be powered via usb, separately from the 24V equipment.

**(5) Hello RuNDMC!**
 - Once the wiring is complete, we should be ready to connect to software. We'll go through this in January / February!

## CAD and CAM

There is a rhino file for perusal under ```/cad``` in the repo. Axis are organized onto layers, and the sets of parts needed for 3D Printing and Milling the machine are present. 

CAM for Fusion360 is located [at this linke](https://a360.co/2lYDSYg)

![beltz](/images/mpvm-rhino-cam.png)

## Assembly 


### The Tools You'll Want

 - soft hammer
 - drill, and driver (nice to have two) 
 - size 34 or equiv. drill bit
 - 3.25mm or close drill bit  (1/8")
 - t15 driver 
 - 2.5mm hex driver
 - 4mm hex driver 
 - clamps? 
 - de-burring tool 
 - flat head screwdriver (playing the role of crowbar)

- Z Gantry 1st
 - bang together and screw

- YZ Block now
 - all hardware should go on (save for motor) before the go-together - but fit up so that you understand orientation
 - socket head cap screw heads should face *towards* other gantry... think of where you'd like more clearance 

 - X Rail, or first, or simultaneous
  - rollers go on
  - beam goes together - try to make it straight!
  - careful on the direction of the back plate - look at the wire routing bit! 

 - Y Gantries / Rollers
  - hardware on, then bang on, then motors. tight screw, but manageable

 - Chassis
  - assy Y rails on sides
  - recommend hitting these edges with the deburring tool 
  - floor, then sides on to floor
  - faces

 - Final Assy 

# BOM

![mpvm-at-mpvhs](/images/mpvm-at-mpvhs.jpg)